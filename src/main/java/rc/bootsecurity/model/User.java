package rc.bootsecurity.model;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull(message = "Name may not be null........")
    @Size(min=10, max=20, message = "Size name 10-20")
    @NotEmpty(message = "Name may not be empty.")
    @Column(nullable = false)
    private String username;

    @NotNull(message = "Name may not be null........")
    @Size(min=3, max=80, message = "Size passw 3-80")
    @NotEmpty(message = "Name may not be empty.")
    @Column(nullable = false)
    private String password;

    private int active;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    //private String permissions = "";

//    public List<String> getRoleList() {
//        if(this.roles.length() > 0){
//            return Arrays.asList(this.roles.split(","));
//        }
//        return new ArrayList<>();
//    }
//
//    public List<String> getPermissionList(){
//        if(this.permissions.length() > 0){
//            return Arrays.asList(this.permissions.split(","));
//        }
//        return new ArrayList<>();
//    }
}
